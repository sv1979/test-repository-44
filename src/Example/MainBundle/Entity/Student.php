<?php

namespace Example\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Student
 *
 * @ORM\Table(name="students")
 * @ORM\Entity(repositoryClass="Example\MainBundle\Entity\StudentRepository")
 * @ORM\HasLifecycleCallBacks
 */
class Student
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;
    /**
     * @var \Date
     *
     * @ORM\Column(name="dob", type="date")
     */
    private $dob;
    /**
     * @var integer
     *
     * @ORM\Column(name="balance", type="integer")
     */
    private $balance;
    private $temp;
    /**
     * @Assert\File(maxSize="6000000")
     */
    private $avatar;
    /**
     * @var ArrayCollection List of courses which student is enrolled in
     * @ORM\ManyToMany(targetEntity="Course", inversedBy="students")
     * @ORM\JoinTable(name="enrollment")
     * @ORM\OrderBy({"name"="ASC"})
     *
     */
    private $courses;

    /**
     * Initializes the entry
     */
    public function __construct()
    {
        $this->courses = new ArrayCollection();
    }

    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir() . '/' . $this->path;
    }

    protected function getUploadDir()
    {
        return 'uploads/documents';
    }

    /**
     * Remove student from specified course
     * @param Course $course
     * @return self
     */
    public function removeCourse(Course $course)
    {
        $this->courses->removeElement($course);
        return $this;
    }

    /**
     * Add student to specified course
     * $param Course $course
     * @return self
     */
    public function addCourse(Course $course)
    {
        $this->courses[] = $course;
        return $this;
    }

    public function hasCourse(Course $course)
    {
        /** @var Course $item */
        foreach ($this->courses as $item) {
            if ($course->getId() == $item->getId()) {
                return true;
            }
        }

        return false;
    }
    /*
     * Returns number of courses for one student
     * @return integer
     */
    public function countCourses(){
        return $this->courses-> count();
    }
    /**
     * Get list of courses where this student is enrolled
     * $return ArrayCollection|Course[]
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Student
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Student
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \Date
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Set dob
     *
     * @param \Date $dob
     * @return Student
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get balance
     *
     * @return integer
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set balance
     *
     * @param integer $balance
     * @return Student
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getAvatar()) {
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename . '.' . $this->getAvatar()->guessExtension();
        }
    }

    /**
     * Gets avatar
     * @return UploadedFile
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Sets avatar
     * @param UploadedFile $avatar
     */
    public function setAvatar(UploadedFile $avatar = null)
    {
        $this->avatar = $avatar;
        if (isset($this->path)) {
            $this->temp = $this->path;
            $this->path = null;
        } else {
            $this->path = 'initial';
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getAvatar()) {
            return;
        }

        $this->getAvatar()->move(
            $this->getUploadRootDir(),
            $this->path
        );

        if (isset($this->temp)) {
            unlink($this->getUploadRootDir() . '/' . $this->temp);
            $this->temp = null;
        }
        $this->file = null;
    }

    public function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir() . '/' . $this->path;
    }

}
