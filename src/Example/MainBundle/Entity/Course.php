<?php

namespace Example\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Course
 *
 * @ORM\Table(name="course")
 * @ORM\Entity(repositoryClass="Example\MainBundle\Entity\CourseRepository")
 * @ORM\HasLifecycleCallBacks
 */
class Course
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var ArrayCollection List of students
     *
     * @ORM\ManyToMany(targetEntity="Student", inversedBy="courses")
     * @ORM\JoinTable(name="enrollment")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $students;

    /**
     * Initializes the entity
     */
    public function __construct()
    {
        $this->students = new ArrayCollection();
    }

    /**
     * Add student to the group
     * @param Student $student
     * return self
     */
    public function addStudent(Student $student)
    {
        $this->students[] = $student;
        return $this;
    }

    public function hasStudent(Student $student)
    {
        /** @var Student $item */
        foreach ($this->students as $item) {
            if ($student->getId() == $item->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Remove student from the group
     * @param Student $student
     * return self
     */
    public function removeStudent(Student $student)
    {
        $this->students-> removeElement($student);
        return $this;
    }

    /**
     * Get list of all students of the group
     * @return ArrayCollection|Student[]
     */
    public function getStudents()
    {
        return $this->students;
    }
    /*
     * Returns number of enrolled students
     * @return integer
     */
    public function countStudents(){
        return $this->students-> count();
    }
    /*
     * Returns balance of enrolled students
     * @return integer
     */
    public function countBalance(){
        $bal = 0;
        $array = $this->students;
        foreach ($array as $item)
        {
            /** @var Student $item */
            $bal = $bal + $item->getBalance();
        }
        return $bal;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Course
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
