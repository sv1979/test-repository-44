<?php

namespace Example\MainBundle\Form;

use Example\MainBundle\Entity\CourseRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StudentType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('name')
            //->add('country')
            ->add('dob','date', array(
                'years' => range(date('Y') -80, date('Y')),
                'data'=> new \DateTime('12 Apr 1990'),
            ))
            ->add('balance')
            ->add('country', 'choice', array(
                'expanded'=>true,
                'choices'  => array(
                    'Austria' => 'Austria',
                    'USA'     => 'United States',
                    'USSR'    => 'Soviet Union',
                    'Germany' => 'Germany',
                ),
            ))
            ->add('avatar')

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Example\MainBundle\Entity\Student'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'example_mainbundle_student';
    }
}
