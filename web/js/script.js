$(document).ready(function(){

    $('.pagination').find(".pager_item").eq(0).addClass('active');
    recountList();

    });
var req_num = 1;

$('#left_arrow').click(function(){
    if (req_num > 1){
        req_num--;
        recountList();
        refreshPager();
    }
});

$('#right_arrow').click(function(){
    if (req_num < total_pages){
        req_num++;
        recountList();
        refreshPager();
    }
});


$('.pager_item').click(function(){

    $('.pager_item').each(function(){
        $(this).removeClass('active');
    });
    $(this).addClass('active');
    req_num = $(this).attr('id');
    recountList();
});

function refreshPager(){
    $('.pager_item').each(function(){
        $(this).removeClass('active');
        if($(this).attr('id')==req_num){
            $(this).addClass('active');
        }
    });

}

function recountList(){
    $('.data_item').each(function(){
        var the_num =  $(this).attr('id');
        if (!(the_num <= req_num * PAGE_SIZE && the_num > PAGE_SIZE * (req_num-1))) {
            $(this).addClass('hidden');
        } else {
            $(this).removeClass('hidden');
        }
    });
}